package servlets;

import classes.Notificacao;
import dao.NotificacaoDao;
import persistence.PersistenceManager;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

@WebServlet("/recebeNotificacao")
public class RecebeNotificacaoServlet extends HttpServlet {
    public RecebeNotificacaoServlet() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {

        PersistenceManager.getInstance().initializeContext();

        Notificacao notificacao;
        NotificacaoDao notificacaoDao = new NotificacaoDao();

        notificacao = notificacaoDao.findById(Integer.parseInt(req.getParameter("cod_moip")), Notificacao.class);

        if (notificacao == null) {
            notificacao = new Notificacao();
            notificacao.setId(Integer.parseInt(req.getParameter("cod_moip")));
            notificacao.setValor(Double.parseDouble(req.getParameter("valor")) / 100.0);
            notificacao.setTipoPagamento(req.getParameter("tipo_pagamento"));
            notificacao.setEmailConsumidor(req.getParameter("email_consumidor"));

            int status = Integer.parseInt(req.getParameter("status_pagamento"));

            switch (status) {
                case 1:
                    notificacao.setStatusTransacao("autorizado");
                    break;
                case 2:
                    notificacao.setStatusTransacao("iniciado");
                    break;
                case 3:
                    notificacao.setStatusTransacao("boleto_impresso");
                    break;
                case 4:
                    notificacao.setStatusTransacao("concluido");
                    break;
                case 5:
                    notificacao.setStatusTransacao("cancelado");
                    break;
                case 6:
                    notificacao.setStatusTransacao("em_analise");
                    break;
                case 7:
                    notificacao.setStatusTransacao("estornado");
                    break;
                default:
                    notificacao.setStatusTransacao("nao_reconhecido");
            }

            notificacaoDao.insert(notificacao);
        } else {
            notificacao.setValor(Double.parseDouble(req.getParameter("valor")) / 100.0);
            notificacao.setTipoPagamento(req.getParameter("tipo_pagamento"));
            notificacao.setEmailConsumidor(req.getParameter("email_consumidor"));

            int status = Integer.parseInt(req.getParameter("status_pagamento"));

            switch (status) {
                case 1:
                    notificacao.setStatusTransacao("autorizado");
                    break;
                case 2:
                    notificacao.setStatusTransacao("iniciado");
                    break;
                case 3:
                    notificacao.setStatusTransacao("boleto_impresso");
                    break;
                case 4:
                    notificacao.setStatusTransacao("concluido");
                    break;
                case 5:
                    notificacao.setStatusTransacao("cancelado");
                    break;
                case 6:
                    notificacao.setStatusTransacao("em_analise");
                    break;
                case 7:
                    notificacao.setStatusTransacao("estornado");
                    break;
                default:
                    notificacao.setStatusTransacao("nao_reconhecido");
            }

            notificacaoDao.update(notificacao, Notificacao.class);
        }

        PersistenceManager.getInstance().destroyContext();
    }
}
