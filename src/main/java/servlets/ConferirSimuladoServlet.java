package servlets;

import classes.Questao;
import dao.QuestaoDao;
import dao.SimuladoDao;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.util.List;

@WebServlet("/conferirSimulado")
public class ConferirSimuladoServlet extends HttpServlet {
    public ConferirSimuladoServlet() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        SimuladoDao simuladoDao = new SimuladoDao();
        QuestaoDao questaoDao = new QuestaoDao();

        int acertos = 0;

        List<Questao> questoes = simuladoDao.carregarListaQuestao(Integer.parseInt(req.getParameter("simuladoId")));

        for (Questao q : questoes) {
            if (q.getTentativa() == null)
                q.setTentativa(0);
            if (q.getAcertos() == null)
                q.setAcertos(0);


            q.setTentativa(q.getTentativa() + 1);
            Integer resposta = Integer.parseInt(req.getParameter(q.getId()));

            if (resposta.equals(q.getResposta())) {
                q.setAcertos(q.getAcertos() + 1);
                acertos++;
            }


            questaoDao.update(q, Questao.class);
        }

        req.setAttribute("acertos", acertos);
        req.setAttribute("questoes", questoes);

        req.getRequestDispatcher("acertos.jsp").forward(req, res);
    }
}
