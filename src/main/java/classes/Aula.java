package classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "aula")
public class Aula extends Entitable<Aula, Date> implements Serializable {

    @Id
    @Temporal(TemporalType.DATE)
    @Column(columnDefinition = "date")
    private Date id;

    @Column(name = "local", length = 40, nullable = false)
    private String local;

    @ManyToOne
    private TipoAula tipoaula;

    @OneToMany(mappedBy = "aula")
    private Set<RlClienteAula> listaCliente;

    @Override
    public Date getId() {
        return id;
    }

    public void setId(Date id) {
        this.id = id;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public TipoAula getTipoAula() {
        return tipoaula;
    }

    public void setTipoAula(TipoAula tipoAula) {
        this.tipoaula = tipoAula;
    }

    public Set<RlClienteAula> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(Set<RlClienteAula> listaCliente) {
        this.listaCliente = listaCliente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aula aula = (Aula) o;
        return Objects.equals(id, aula.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String getTableName() {
        return "Aula";
    }

    @Override
    public void clonar(Aula objeto) {
        setLocal(objeto.getLocal());
        setTipoAula(objeto.getTipoAula());
        setListaCliente(objeto.getListaCliente());
    }
}