package classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "questao")
public class Questao extends Entitable<Questao, String> implements Serializable {
    @Id
    @Column(name = "enunciado", length = 700, nullable = false)
    private String id;

    @Column(name = "itema", length = 700)
    private String itemA;

    @Column(name = "itemb", length = 700)
    private String itemB;

    @Column(name = "itemc", length = 700)
    private String itemC;

    @Column(name = "itemd", length = 700)
    private String itemD;

    @Column(name = "itemE", length = 700)
    private String itemE;

    @Column(name = "resposta")
    private Integer resposta;

    @Column(name = "tentativas")
    private Integer tentativa;

    @Column(name = "acertos")
    private Integer acertos;

    @ManyToMany(mappedBy = "listaQuestao")
    private Set<Simulado> listaSimulado;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemA() {
        return itemA;
    }

    public void setItemA(String itemA) {
        this.itemA = itemA;
    }

    public String getItemB() {
        return itemB;
    }

    public void setItemB(String itemB) {
        this.itemB = itemB;
    }

    public String getItemC() {
        return itemC;
    }

    public void setItemC(String itemC) {
        this.itemC = itemC;
    }

    public String getItemD() {
        return itemD;
    }

    public void setItemD(String itemD) {
        this.itemD = itemD;
    }

    public String getItemE() {
        return itemE;
    }

    public void setItemE(String itemE) {
        this.itemE = itemE;
    }

    public Integer getResposta() {
        return resposta;
    }

    public void setResposta(Integer resposta) {
        this.resposta = resposta;
    }

    public Integer getTentativa() {
        return tentativa;
    }

    public void setTentativa(Integer tentativa) {
        this.tentativa = tentativa;
    }

    public Integer getAcertos() {
        return acertos;
    }

    public void setAcertos(Integer acertos) {
        this.acertos = acertos;
    }

    public Set<Simulado> getListaSimulado() {
        return listaSimulado;
    }

    public void setListaSimulado(Set<Simulado> listaSimulado) {
        this.listaSimulado = listaSimulado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Questao questao = (Questao) o;
        return Objects.equals(id, questao.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String getTableName() {
        return "Questao";
    }

    @Override
    public void clonar(Questao objeto) {
        if (objeto.getItemA() != null)
            setItemA(objeto.getItemA());

        if (objeto.getItemB() != null)
            setItemB(objeto.getItemB());

        if (objeto.getItemC() != null)
            setItemC(objeto.getItemC());

        if (objeto.getItemD() != null)
            setItemD(objeto.getItemD());

        if (objeto.getItemE() != null)
            setItemE(objeto.getItemE());

        if (objeto.getResposta() != null)
            setResposta(objeto.getResposta());

        if (objeto.getTentativa() != null)
            setTentativa(objeto.getTentativa());

        if (objeto.getAcertos() != null)
            setAcertos(objeto.getAcertos());
    }
}
