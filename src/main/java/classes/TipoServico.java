package classes;

import classes.enums.TipoServicoNome;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name = "tipo_servico")
public class TipoServico extends Entitable<TipoServico, Integer> implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(name = "tipo_servico", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoServicoNome tipo;

    private Double preco;

    @OneToMany(mappedBy = "tipo", targetEntity = Servico.class,cascade = CascadeType.ALL)
    private Set<Servico> listaServicos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer idTipoServico) {
        this.id = idTipoServico;
    }

    public TipoServicoNome getTipo() {
        return tipo;
    }

    public void setTipo(TipoServicoNome tipo) {
        this.tipo = tipo;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Set<Servico> getListaServicos() {
        return this.listaServicos;
    }

    public void setListaServicos(Set<Servico> listaServicos) {
        this.listaServicos = listaServicos;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        TipoServico other = (TipoServico) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String getTableName() {
        return "TipoServico";
    }

    @Override
    public void clonar(TipoServico objeto) {
        if(objeto.getPreco() != null)
            setPreco(objeto.getPreco());

        if(objeto.getTipo() != null)
            setTipo(objeto.getTipo());
    };
}
