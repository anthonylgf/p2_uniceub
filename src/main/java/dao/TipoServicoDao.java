package dao;

import classes.TipoServico;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class TipoServicoDao extends DaoGeral<TipoServico, Integer> {
    public TipoServicoDao() {
        super();
    }

    public Integer findIdByName(String name) {
        Integer retorno;

        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        String sql = "select p from TipoServico p where tipo_servico = :tipoparametro";
        Query query = entityManager.createQuery(sql);
        query.setParameter("tipoparametro", name);

        List<TipoServico> listaRetorno = query.getResultList();

        if(listaRetorno.size() == 0){
            retorno = -1;
        } else {
            retorno = listaRetorno.get(0).getId();
        }

        transaction.commit();

        entityManager.close();

        return retorno;
    }
}