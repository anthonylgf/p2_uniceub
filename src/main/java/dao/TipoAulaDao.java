package dao;

import classes.TipoAula;
import classes.TipoServico;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class TipoAulaDao extends DaoGeral<TipoAula, Integer> {
    public TipoAulaDao() {
        super();
    }

    public Integer findIdByName(String name) {
        Integer retorno;

        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        String sql = "select p from TipoAula p where tipo = :tipoparametro";
        Query query = entityManager.createQuery(sql);
        query.setParameter("tipoparametro", name);

        List<TipoAula> listaRetorno = query.getResultList();

        if(listaRetorno.size() == 0){
            retorno = -1;
        } else {
            retorno = listaRetorno.get(0).getId();
        }

        transaction.commit();

        entityManager.close();

        return retorno;
    }
}
