<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>Simulado!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/pagina-simulado.css">
</head>
<body>
<header class="w3-container w3-blue w3-center" style="padding:128px 16px">
    <h1>Simulado</h1>
    <h2 class="w3-padding-32" align="center">
        Faça o simulado!
    </h2>
</header>

<div class="w3-row-padding w3-padding-64 w3-container">
    <div class="w3-content">
        <div class="w3-center" align="center">
            <form action="/conferirSimulado" method="post">
                <table id="tabela-simulado">
                    <c:forEach var="questao" items="${requestScope.questoes}" varStatus="count">
                        <tr>
                            <td colspan="3" align="center"><h5>Questão ${count.count}</h5></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <p>${questao.id}</p>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="${questao.id}" value="1" required> ${questao.itemA}</td>
                            <td><input type="radio" name="${questao.id}" value="2"> ${questao.itemB}</td>
                            <td><input type="radio" name="${questao.id}" value="3"> ${questao.itemC}</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="${questao.id}" value="4"> ${questao.itemD}</td>
                            <td><input type="radio" name="${questao.id}" value="5"> ${questao.itemE}</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <hr/>
                            </td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td>
                            <input type="hidden" value="${requestScope.id}" name="simuladoId">
                            <input type="submit" class="w3-button w3-green" value="Finalizar">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

</body>
</html>
